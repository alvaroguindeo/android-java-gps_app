package com.example.ejemplogpsmaps;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class PasoIntermedio extends AppCompatActivity {

    public TextView usuario,contra,revisor;
    public Button loguin,crear,edicion;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paso_intermedio);
        usuario=findViewById(R.id.editText);
        contra=findViewById(R.id.editText3);
        crear=findViewById(R.id.button5);
        loguin=findViewById(R.id.button4);
        edicion=findViewById(R.id.button6);
        revisor=findViewById(R.id.textView8);
    }

    public void alta(View v) {
        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(this,
                "administracion", null, 1);
        SQLiteDatabase bd = admin.getWritableDatabase();
        String nombres = usuario.getText().toString();
        String contrasenas = contra.getText().toString();
       // String pre = et3.getText().toString();
        ContentValues usuarios = new ContentValues();
        usuarios.put("nombre", nombres);
        usuarios.put("contrasena", contrasenas);
       // usuarios.put("precio", pre);
        bd.insert("usuarios", null, usuarios);
        bd.close();
        usuario.setText("");
        contra.setText("");
        //et3.setText("");
        Toast.makeText(this, "Se cargaron los datos del usuario",
                Toast.LENGTH_SHORT).show();
      //  bd.close();
    }
    public void consultaporcodigo(View v) {
        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(this,
                "administracion", null, 1);
        SQLiteDatabase bd = admin.getWritableDatabase();
        String cod = usuario.getText().toString();
        Cursor fila = bd.rawQuery(
                "select contrasena from usuarios where nombre='" + cod +"'", null);
        if (fila.moveToFirst()) {
            revisor.setText(fila.getString(0));


        }

        if (revisor.getText().toString()!=contra.getText().toString()){
            edicion.setVisibility(View.VISIBLE);
            Toast.makeText(this, "Usuario Correcto",
                    Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(this, "usuario incorrecto",
                    Toast.LENGTH_SHORT).show();
        }
        bd.close();
    }


    public void cambio(View v){
        Intent i = new Intent(this, CambioDatos.class );
        startActivity(i);
    }
}
