package com.example.ejemplogpsmaps;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class CambioDatos extends AppCompatActivity {
    public TextView Nnombre,Nurl;
    public Button vuelta;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cambio_datos);
        Nnombre=findViewById(R.id.nn);
        Nurl=findViewById(R.id.mm);
        vuelta=findViewById(R.id.bb);
    }

    public void volver(View v){
        Intent i = new Intent(this, MainActivity.class );
        i.putExtra("nombre", Nnombre.getText().toString());
        i.putExtra("direccion", Nurl.getText().toString());
        startActivity(i);
    }


}
